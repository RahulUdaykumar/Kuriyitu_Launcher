
# Kuṟiyīṭu Launcher

It is a small chrome extension and an assignment for elocals company

## Description

This extension is a launcher which would be handy for programmer/coders

### Prerequisite

Use chrome in developers mode

### Setup

clone the directory

Follow the below Steps

```
Step 1: Open Chrome browser
```
```
Step 2: Open Chrome Settings and click on Extensions
```
```
Step 3: Select Load Unpacked
```
```
Step 4: Choose the cloned folder or the extension
```
```
Step 5: Now you can see an extension added to your browser
```
```
Step 6: Make use of it 
```